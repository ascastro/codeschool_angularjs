(function() {
	var app = angular.module('store', []);

	app.controller('StoreController', function() {
		this.products = gems;
	});

	app.controller('PanelController', function() {
		this.tab = 1;

		this.selectTab = function(setTab) {
			this.tab = setTab;
		}

		this.isSelected = function(checkTab) {
			return this.tab === checkTab;
		}
	});

	app.controller('ReviewController', function() {
		this.review = {};

		this.addReview = function(product) {
			product.reviews.push(this.review);

			this.review = {};
		};
	});

	app.directive('productTitle', function () {
		return {
			restrict: 'E',	// type of directive - E is for element
			templateUrl: 'product-title.html'
		};
	});

	app.directive('productPanels', function() {
		return {
			restrict: 'E',
			templateUrl: 'product-panels.html',
			controller: function() {
				this.tab = 1;

				this.selectTab = function(setTab) {
					this.tab = setTab;
				}

				this.isSelected = function(checkTab) {
					return this.tab === checkTab;
				}
			},

			controllerAs: 'panel'
		};
	});

	var gems = [
		{
			name: 'Dodecahedron',
			price: 2.95,
			description: '...',
			canPurchase: true,
			soldOut: false,
			images: [
				{
					full: 'dodecahedron-01-full.jpg',
					thumb: 'Dodecahedron-01-thumb.jpg'
				},
				{
					full: 'dodecahedron-02-full.jpg',
					thumb: 'Dodecahedron-02-thumb.jpg'
				}
			],
			reviews: [
				{
					stars: 5,
					body: 'I love it',
					author: 'example@example.org'
				},
				{
					stars: 5,
					body: 'I love it',
					author: 'example@example.org'
				},
				{
					stars: 5,
					body: 'I love it',
					author: 'example@example.org'
				}
			]
		},
		{
			name: 'Pentagonal Gem',
			price: 5.95,
			description: '...',
			canPurchase: true,
			soldOut: false,
			images: [
				{
					full: 'pentagonal-01-full.jpg',
					thumb: 'pentagonal-01-thumb.jpg'
				}
			]
		}
	];
})();